---
title: "Software Innovation Campus Paderborn"
link: https://www.sicp.de/
weight: 3
resources:
- name: logo
  src: sicp.png
---

The SICP provides us an own room and seminar rooms (for the participation in CTFs).